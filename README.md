# README #

This project contains the Scala porting of the service described in [this](http://www.smartjava.org/content/create-simpe-restful-service-vertx-20-rxjava-and-mongodb) tutorial.

Branch *rx-extension* contains the update operation based on RxScala Observable. In order to achieve this I did a very naive porting of mod-rxjava for rxscala limited to the functionality required for this simple example.

## Launch the application with SBT ##

Adopted environment: 

* SBT version 0.13.8
* openjdk version "1.8.0_45"
* mongodb version v2.4.13

```
#!

$ sbt 'run-main org.vertx.java.platform.impl.cli.Starter run scala:com.example.ZipServer -conf src/main/resources/config.json'
```
Remember to start mongodb before shooting requests.

## Test with curl ##

Get all:

```
#!bash

curl -H "Content-Type: application/json" -X GET http://localhost:8080/zips
```

Get many (by city or state):
```
#!bash

curl -H "Content-Type: application/json" -X GET http://localhost:8080/zips?city=YORK

curl -H "Content-Type: application/json" -X GET http://localhost:8080/zips?state=AL
```

Get by id:

```
#!bash

curl -H "Content-Type: application/json" -X GET http://localhost:8080/zips/99950
```

Update:

```
#!bash

curl -H "Content-Type: application/json" -X POST -d '{"_id":"06410","city":"NEW","state":"XX"}' http://localhost:8080/zips/06410
```
