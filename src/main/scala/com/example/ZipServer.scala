/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example

import org.vertx.scala.core.eventbus.Message
import org.vertx.scala.core.http.{HttpServerRequest, RouteMatcher}
import org.vertx.java.core.http.{ RouteMatcher => JRouteMatcher }
import org.vertx.scala.core.json.{JsonArray, Json, JsonObject}
import org.vertx.scala.platform.Verticle
import scala.collection.JavaConversions._

class ZipServer extends Verticle {
  private val port = 8080

  override def start() {

    val appConfig = container config()
    container.deployModule("io.vertx~mod-mongo-persistor~2.1.1", appConfig.getObject("mongo-persistor"))


    val routeMatcher = RouteMatcher(new JRouteMatcher())

    routeMatcher.get("/zips", {req : HttpServerRequest =>
      val json = Json.obj(
        "action" -> "find",
        "collection" -> "zips",
        "matcher" -> Json.obj(req.params.iterator()
          .filter(entry => Set("state", "city")
          .contains(entry.getKey()))
          .map(entry => (entry.getKey -> entry.getValue))
          .toSeq:_*)
      )
      def handleMessage(data : JsonArray)(msg : Message[JsonObject]): Unit = {
        data.add(msg.body().getArray("results"))
        val status = msg.body.getString("status")
        logger info s"Received message with status: $status"
        if (status.equals("more-exist")) {
          msg.reply(Json.emptyObj(), handleMessage(data))
        } else {
          req.response().putHeader("Content-Type", "application/json")
          req.response().end(Json.obj("results" -> data).encodePrettily())
        }
      }
      container.logger info s"JSon request : $json"

      vertx.eventBus.send("mongodb-persistor", json : JsonObject, handleMessage(Json.arr()))
    })
    routeMatcher.get("/zips/:id", {req : HttpServerRequest =>
      val json = Json.obj(
        "action" -> "findone",
        "collection" -> "zips",
        "matcher" -> Json.obj("_id" -> req.params.get("id"))
      )
      container.logger info s"JSon request : $json"

      vertx.eventBus.send("mongodb-persistor", json : JsonObject, {msg : Message[JsonObject] => {
        req.response().putHeader("Content-Type", "application/json")
        req.response().end(msg.body().encodePrettily())
      }})
    })

    vertx.createHttpServer.requestHandler(routeMatcher).listen(port)

    container.logger info s"Webserver started at $port!"

  }
 
}